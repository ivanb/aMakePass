#!/bin/ksh

# Intellectual property information START
# 
# Copyright (c) 2020 Ivan Bityutskiy 
# 
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 
# Intellectual property information END

# Description START
#
# The script creates text file aPasswdsMore.txt
# with 100 passwords of user defined length.
# If invalid length is specified (>128 or <1),
# the value 30 is used.
# Script is 'slow' (128 symbols passwords in 0.50 seconds)
#
# Description END

# Shell settings START
set -o noglob
# Shell settings END

# Define functions START
function testSetPL
{
  # Limit user input to 3 symbols
  local -R3 trimInput="$1"
  [[ "$trimInput" == *([[:blank:]])+([[:digit:]]) ]] &&
    (( $trimInput <= 128 && $trimInput >= 1 )) &&
      (( passLength=$trimInput ))
}
# Define functions END

# Declare variables START
integer passLength=30 num
typeset arrSymbols rnd1='/' rnd2='/' rnd3='/' rnd4='/'
# 100 symbol array (array element from 0 to 99),
#  90 main symbols (array element from 0 to 89)
set -A arrSymbols -- '!' $(jot -c '-' 35 38
jot -c '-' 40 91
jot -c '-' 93 95
jot -c '-' 97 126
jot -c 6 64 58
jot -c 4 126 123)
# Declare variables END

# BEGINNING OF SCRIPT

# Setting random values for variables rnd1, rnd3
# Symbol / in rnd[1234] variables interfere with sed
while [ $rnd1 == / ]
do
  rnd1="${arrSymbols[RANDOM%100]}"
done

while [ $rnd3 == / ]
do
  rnd3="${arrSymbols[RANDOM%100]}"
done

# Test user input and determine variable passLength
(( $# == 1 )) && testSetPL "$1" ||
  {
    print -n -- "Enter password's length: "
    read --
    testSetPL "$REPLY"
  }

# Setting random values for variables rnd2, rnd4
while [ $rnd2 == / ]
do
  rnd2="${arrSymbols[RANDOM%100]}"
done

while [ $rnd4 == / ]
do
  rnd4="${arrSymbols[RANDOM%100]}"
done

# Read random numbers from jot, use them to withdraw a
# value from the array and pass it to rs for processing,
# run sed to replace duplicated symbols,
# then save result into aPasswdsMore.txt, overwriting it.
# jot needs floats to better distribute first and last
# value in range. rs -g0 removes spaces between fields.
jot -rw '%d' ${passLength}00 '0.01' '89.99' |
  while read -- num
  do
    print -r -- "${arrSymbols[num]}"
  done |
    rs -g0 0 $passLength |
    sed -e "s/\(.\)\1/${rnd1}${rnd2}/g" -e "s/\(.\)\1/${rnd3}${rnd4}/g" >| aPasswdsMore.txt

# Shell settings START
set +o noglob
# Shell settings END

# END OF SCRIPT

