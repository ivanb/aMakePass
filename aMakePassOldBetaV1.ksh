#!/bin/ksh

# Intellectual property information START
# 
# Copyright (c) 2020 Ivan Bityutskiy 
# 
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 
# Intellectual property information END

# Description START
#
# !!! Old BETA version !!!
# The script creates text file aPasswdsMore.txt
# with 100 passwords of user defined length.
# If invalid length is specified (>128 or <1),
# the value 30 is used.
# Script is 'slow' (128 symbols file made in 1.40 seconds)
#
# Description END

# Shell settings START
set -o noglob
# Shell settings END

# Define functions START
function testSetPL
{
  # Limit user input to 3 symbols
  local -R3 trimInput="$1"
  [[ "$trimInput" == *([[:blank:]])+([[:digit:]]) ]] &&
    (( $trimInput <= 128 && $trimInput >= 1 )) &&
      (( passLength=$trimInput ))
}
# Define functions END

# Declare variables START
integer passLength=30 num counter=1
typeset arrSymbols rnd1='/' rnd2='/' passTxt=''
# 100 symbol array (from 0 to 99),
#  90 main symbols (from 0 to 89)
set -A arrSymbols -- '!' '#' '$' '%' '&' '(' ')' '*' '+' ',' '-' '.' '/' '0' '1' '2' '3' '4' '5' '6' '7' '8' '9' ':' ';' '<' '=' '>' '?' '@' 'A' 'B' 'C' 'D' 'E' 'F' 'G' 'H' 'I' 'J' 'K' 'L' 'M' 'N' 'O' 'P' 'Q' 'R' 'S' 'T' 'U' 'V' 'W' 'X' 'Y' 'Z' '[' ']' '^' '_' 'a' 'b' 'c' 'd' 'e' 'f' 'g' 'h' 'i' 'j' 'k' 'l' 'm' 'n' 'o' 'p' 'q' 'r' 's' 't' 'u' 'v' 'w' 'x' 'y' 'z' '{' '|' '}' '~' '@' '>' '=' '<' ';' '9' '~' '}' '|' '{'
# Declare variables END

# BEGINNING OF SCRIPT

# Symbol / in rnd[12] variables interfere with sed
while [ $rnd1 == / ]
do
  rnd1="${arrSymbols[RANDOM%100]}"
done

(( $# == 1 )) && testSetPL "$1" ||
  {
    print -n -- "Enter password's length: "
    read --
    testSetPL "$REPLY"
  }

while [ $rnd2 == / ]
do
  rnd2="${arrSymbols[RANDOM%100]}"
done

# Read random numbers from jot, use them to withdraw a
# value from the array and populate passTxt variable.
while read -- num
do
  if (( ! (counter % passLength) &&
    counter++ != ${passLength}00 ))
  then
    passTxt="${passTxt}${arrSymbols[num]}\n"
  else
    passTxt="${passTxt}${arrSymbols[num]}"
  fi
done <<EOF
$(jot -w '%d' -r ${passLength}00 '0.01' '89.99')
EOF

# Run sed to replace duplicated symbols,
# then save result into aPasswdsMore.txt, overwriting it.
print -- $passTxt | sed "s/\(.\)\1/${rnd1}${rnd2}/g" >| aPasswdsMore.txt

# Shell settings START
set +o noglob
# Shell settings END

# END OF SCRIPT

