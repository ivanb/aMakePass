# aMakePass

pdksh scripts create text file with 100 random passwords.

Password length is specified by the user (from 1 to 128), or 30.

aMakePassMoreFast.ksh uses all adequate symbols for password generation.

aMakePassAlnum.ksh uses only alphanumeric symbols.

Other scrips in this repository are either less effective of beta versions.

Scripts are intended to run on OpenBSD system.

Dependencies: jot, rs. Both are present in OpenBSD base.

**jot** in linux is **bugged**